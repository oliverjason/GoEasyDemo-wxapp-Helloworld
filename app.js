//app.js
import GoEasy from './utils/goeasy-1.0.17';
App({
  onLaunch: function () {
    this.extendDateFormat();
    this.initGoEasy();
  },
  extendDateFormat () {
    Date.prototype.formatDate = function (fmt) {
      var o = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S": this.getMilliseconds()
      };
      if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
      for (var k in o)
        if(o.hasOwnProperty(k)){
          if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
      return fmt;
    };
  },
  initGoEasy() {//初始化goeasy
      this.globalData.goeasy = GoEasy({
          host: 'hangzhou.goeasy.io',
          appkey: "BC-bf378cd626c84ed98ec9727f44601175",
          onConnected: function () {
              console.log("GoEasy connect successfully.");
          },
          onDisconnected: function () {
              console.log("GoEasy disconnected.")
          },
          onConnectFailed: function (error) {
              console.log('连接失败，请检查您的appkey和host配置');
          }
      })
  },
  globalData : {
    goeasy : null
  }
})