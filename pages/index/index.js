//index.js
//获取应用实例

const app = getApp();
Page({
    data: {
        goeasy: null,
        messages: [],
        message: ""
    },
    onLoad: function () {
        this.subscribeMessage();
    },
    /**
     * 
     * @param {String} content 内容
     * @param {String} type 类型，发送方 send、接收方 recive
     */
    pushMessage(content, type) {
        var formattedTime = new Date().formatDate("hh:mm");
        var message = {
            content: formattedTime +" "+ content,
            type
        };
        var messages = this.data.messages;
        messages.push(message);
        this.setData({
            messages: messages
        })
    },
    // 接收channel方发送的消息
    subscribeMessage() {
        var self = this;
        app.globalData.goeasy.subscribe({
            channel: "my_channel2",
            onMessage: function (message) {
                self.pushMessage(message.content, 'recive');
            },
            onSuccess: function () {
                console.log('建立会话成功')
            }
        });
    },
    sendMessage: function () {//发送消息
        var self = this;
        var content = this.data.message;
        if (content.trim() != '') {
            app.globalData.goeasy.publish({
                channel: "my_channel1",
                message: content,
                onSuccess: function () {
                    self.setData({
                        message: ''
                    }); //清空发送消息内容
                    self.pushMessage(content, 'send')
                    // console.log("send message success");
                },
                onFailed: function (error) {
                    console.log('发送失败，请检查您的appkey和host配置.')
                }
            });
        }
    }
})
