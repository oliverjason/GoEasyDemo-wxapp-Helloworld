# 原生微信小程序使用GoEasy快速实现websocket实时通讯


本教程和全套源码已经开源在OSChina码云上，会持续更新，最新版本请访问[https://gitee.com/goeasy-io/GoEasyDemo-wxapp-Helloworld](https://gitee.com/goeasy-io/GoEasyDemo-wxapp-Helloworld)

不需要下载安装，便可以在微信好友、微信群之间快速的转发，用户只需要扫码或者在微信里点击，就可以立即运行，有着近似APP的用户体验，使得微信小程序成为全民热爱的好东西~

同时因为微信小程序使用的是Javascript语法，对前端开发人员而言，几乎是没有学习成本和技术门槛的。对于大部分场景，都可以使用小程序快速开发实现，不论是开发周期还是开发成本都低的让人笑哭，所以受到了技术开发团队的各种追捧~

但如果要在小程序里快速的实现一个即时通讯功能，就有点尴尬，因为微信官方提供的只是一个底层的websocket api，要在项目中直接使用，还需要做很多额外的工作，比如首先就需要搭建自己的websocket服务~ 

那有没有简单的方式呢？ 当然是有的！

今天小编就手把手的教您用GoEasy在微信小程序里，最短的时间快速实现一个websocket即时通讯Demo。

![image](./wxapp-helloworld-gif.gif)

本demo已经完成了真机下的小程序的测试，完整源代码开源到oschina的码云上，clone后，只需要将代码里的appkey换成自己的common key，就可以体验了, 源码网址：[https://gitee.com/goeasy-io/GoEasyDemo-wxapp-Helloworld](https://gitee.com/goeasy-io/GoEasyDemo-wxapp-Helloworld)

# 1、获取appkey
GoEasy官网（[https://www.goeasy.io/](https://www.goeasy.io/)）上注册账号，创建一个应用，拿到您的appkey。
 ![image](https://uploader.shimo.im/f/YAeaGU0CE83jcEeB.png)
 
**GoEasy提供了两种类型的appkey：**

- Common key: 即可以接收消息，也可以发送消息，与Subscribe Key最大的区别就是有写权限，可以发消息。适用于有消息发送需求的客户端和服务端开发。

- Subscribe key: 只能接收消息，不可以发送消息，与Common Key最大的区别就是没有写权限，只能收消息。可以用于一些没有发送需求的客户端。

# 2、获取GoEasy SDK
下载 [https://cdn.goeasy.io/download/goeasy-1.0.11.js](https://cdn.goeasy.io/download/goeasy-1.0.11.js)

```
import GoEasy from './goeasy-1.0.11';
```

# 3、初始化GoEasy对象

```
var self = this;
this.goeasy = GoEasy({
    host: 'hangzhou.goeasy.io',
    appkey: "您的appkey",
    onConnected: function () {
        console.log("GoEasy connect successfully.");
        self.unshiftMessage("连接成功.");
    },
    onDisconnected: function () {
        console.log("GoEasy disconnected.")
        self.unshiftMessage("连接已断开.");
    },
    onConnectFailed: function (error) {
        console.log(error);
        self.unshiftMessage("连接失败，请检查您的appkey和host配置");
    }
})
```


根据您在GoEasy后台创建应用时选择的区域，来传入不同的Host,如果您创建GoEasy应用时,选择了杭州，那么host:"hangzhou.goeasy.io"。选择了新加坡,host:"singapore.goeasy.io"。

如果您的大部分用户都是在国内，创建应用时，记得选择杭州，以便获得更快的通讯速度。

# 4、小程序端接收消息

```
var self = this;
this.goeasy.subscribe({
    channel: "my_channel",
    onMessage: function (message) {
        self.unshiftMessage(message.content);
    },
    onSuccess: function () {
        self.unshiftMessage('订阅成功.');
    }
});
```


很多朋友会问channel从哪里来，如何创建，应该传入什么呢？

根据您的业务需求来设定，channel可以为任意字符串，除了不能包含空格，和不建议使用中文外，没有任何限制，只需要和消息的发送端保持一致，就可以收到消息。channel可以是您直播间的uuid,也可以是一个用户的唯一表示符，可以任意定义，channel不需要创建，可以随用随弃。
# 5、小程序端发送消息：
发送时，需要注意channel一定要和subscribe的channel完全一致，否则无法收到。

```
this.goeasy.publish({
    channel: "my_channel",
    message: self.data.message,
    onSuccess: function () {
        self.setData({
            message: ''
        }); //清空发送消息内容
        console.log("send message success");
    },
    onFailed: function (error) {
        self.unshiftMessage('发送失败，请检查您的appkey和host配置.');
    }
});
```

本代码源码下载：[https://gitee.com/goeasy-io/GoEasyDemo-wxapp-Helloworld](https://gitee.com/goeasy-io/GoEasyDemo-wxapp-Helloworld)

**特别强调：**

在运行之前，一定要在微信公众号平台配置socket合法域名，否则无法建立连接。

具体步骤：
  
访问https://mp.weixin.qq.com，进入微信公众平台|小程序 -> 设置 -> 开发设置 -> 服务器域名  
socket合法域名-> 添加GoEasy的地址： wx-hangzhou.goeasy.io（记得wx-开头）  
若您创建GoEasy应用时选择了新加坡区域则添加地址：wx-singapore.goeasy.io


# 答疑时间：
#### 1、我的服务器端可以给小程序发送消息吗？都支持些哪些语言？
当然可以，任何语言都可以通过调用GoEasy的Rest API发送消息，同时为了大家方便，GoEasy的官方文档里，也准备了Java, C#,NodeJS，PHP，Ruby和Python等常见语言调用REST API的代码，这里获取更多详情：[https://www.goeasy.io/cn/doc/server/publish.html](https://www.goeasy.io/cn/doc/server/publish.html)

#### 2、GoEasy可以发送图片，语音和视频吗？
当然可以，您可以通过推送文件路径的方式来实现文件的发送。
按照行业惯例，不论MSN，微信，QQ对于图片和视频，通常的做法都是，只推送文件路径，而不会推送文件本身。你如果有注意的话，当您接受图片和视频的时候，收到消息后，等一会儿才能看，就是因为发送的时候，只发送了路径。

#### 3、GoEasy和微信小程序官方的websocket API有什么区别和优势？
小程序官方的websocket API主要是用来与您的websocket服务通讯，所以使用小程序websocket的前提是，首先要搭建好您自己的websocket服务，然后与之通讯。这是一个纯技术的API，在建立网络连接后，还有很多的工作需要自己来完成，比如：
- 需要自己实现心跳机制，来维护网络连接，来判断客户端的网络连接状态；
- 需要自己实现断网自动重连；
- 需要自己维护消息列表，确保遇到断网重连后，消息能够补发；
- 需要自己维护一个客户端列表；
- 等等很多细致而繁杂的工作，比如websocket的安全机制和性能优化；

此之外服务端也有很多工作需要自己完成，有兴趣自己搭建websocket的话，可以参考这篇技术分享《[搭建websocket消息推送服务，必须要考虑的几个问题](https://)》

而GoEasy是一个成熟稳定的企业级websocket PAAS服务平台，开发人员不需要考虑websocket服务端的搭建，只需要几行代码，就可以轻松实现客户端与客户端之间，服务器与客户端之间的的websocket通信，不需要考虑性能，安全，高可用集群的问题,只需要全力专注于开发自己的业务功能就好了。

GoEasy已经内置websocket中必备的心跳，断网重连，消息补发，历史消息和客户端上下线提醒等特性，开发人员也不需要自己搭建websocket服务处理集群高可用，安全和性能问题。GoEasy已经稳定运行了5年，支持千万级并发，成功支撑过很多知名企业的重要活动，安全性和可靠性都是久经考验。

#### 4、GoEasy在小程序的开发中主要用在那些场景呢？
从应用场景上来说，所有需要websocket通信的场景，GoEasy都可以完美支持：
- 聊天，IM，直播弹幕，用户上下线提醒, 在线用户列表
- 扫码点菜，扫码登录， 扫码支付， 扫码签到， 扫码打印
- 事件提醒，工单，订单实时提醒
- 在线拍卖， 在线点餐，在线选座 实时数据展示，实时监控大屏， 金融实时行情显示，设备监控系统
- 实时位置跟踪，外卖实时跟踪，物流实时跟踪
- 远程画板，远程医疗，游戏，远程在线授课

#### 5、GoEasy的文档为什么这么简单？简单到我都不知道如何使用
简单还不好吗？GoEasy从研发的第一天，就把追求API的极简作为我们的工作重点。严格控制接口的数量，就是是为了降低开发人员的学习成本，其实就是为了让您爽啊！但这并不影响GoEasy完美支持所有的websocket即时通讯需求。


